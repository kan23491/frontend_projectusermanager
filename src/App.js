import React from 'react';
import './App.css';
import Loging from './component/Login'
import UserManage from './component/UserManage';
import {BrowserRouter, Route} from 'react-router-dom'
import AddUser from './component/AddUser';
import EditUser from './component/EditUser';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <div>
            <Route exact path="/" component={Loging} />
            <Route exact path="/UserManage" component={UserManage} />
            <Route exact path="/AddUser" component={AddUser} />
            <Route exact path="/EditUser" component={EditUser} />
        </div>
      </BrowserRouter>
      
    </div>
  );
}

export default App;
