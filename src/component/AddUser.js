import React, { useState } from 'react';
import {Link} from 'react-router-dom';
import './staly.css';
import { Button } from 'antd';

class AddUser extends React.Component {
    constructor() {
        super();
        this.state = {
            user:[]
        }
    }

    componentDidMount() {
        console.log('Componnet');
    }

    addnew(event){
        var that = this;
        event.preventDefault();
        let data = {
            name: this.refs.name.value,
            email: this.refs.email.value,
            department: this.refs.department.value,
            job: this.refs.job.value,
            password: this.refs.password.value,
            role: this.refs.role.value,
            user_id: Math.random().toFixed(1)
        };

        // console.log(user_data)
        var request = new Request('http://localhost:3001/api/new-user', {
            method: 'POST',
            headers: new Headers({'Content-Type': 'application/json'}),
            body: JSON.stringify(data)
        });

        let user = that.state.user;
        user.push(data);
        that.setState({
            user: user
        })


        fetch(request)
        .then(function(response) {
            response.json()
            .then(function(data) {
               console.log(data)
            })
        })
        .catch(function(err) {
            console.log(err)
        })
    }

    render() {
        let user = this.state.user;
        return (
            <div>
                <div className="haeder">
                    <h2>Add New</h2>
                </div>
                <div className="form">
                   <div className="box">
                        {/* <div className="name">
                            <h3 >Number</h3>
                            <h3 className="required"> *</h3>
                            <input type="text" required  ref="user_id"/>
                        </div> */}
                        <div className="name">
                            <h3 >Name</h3>
                            <h3 className="required"> *</h3>
                            <input type="text" required  ref="name"/>
                        </div>
                    <div className="name">
                           <h3 >Email</h3>
                           <h3 className="required"> *</h3>
                           <input type="email" required ref="email"/>
                       </div>
                      <div className="name">
                           <h3 >Department</h3>
                           <h3 className="required"> *</h3>
                           <select name="department">
                                <option ref="department">IT</option>
                                <option ref="department">acc</option>
                           </select>
                       </div>
                       <div className="name">
                           <h3 >Job title</h3>
                           <h3 className="required"> *</h3>
                           <input type="text" required ref="job"/>
                       </div>
                       <div className="name">

                           <h3 >Password</h3>
                           <h3 className="required"> *</h3>
                           <input type="password" required ref="password"/>
                       </div>
                       <div className="name">
                           <p className="role">Role</p>
                           <input type="checkbox" className="check" ref="role" value="admin"/> Admin
                           <input type="checkbox" className="check" ref="role" value="manage"/> Manage
                       </div>
                       
                       <div className="button">
                       <Button onClick={this.addnew.bind(this)} type="primary" className="btnSave"> SAVE</Button>
                       <Button danger type="primary"><Link to="/UserManage" className="btnCancel">CANCEL</Link></Button>
                       
                       </div>
                   </div>
                </div>
            </div>
        )
    }
}

export default AddUser;