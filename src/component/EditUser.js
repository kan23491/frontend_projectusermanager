import React, { useState } from 'react';
import './staly.css';
import {Link} from 'react-router-dom';
import './staly.css';
import { Button } from 'antd';
import {Redirect} from 'react-router-dom';

class AddUser extends React.Component {
    constructor() {
        super();
        this.state = {
            user:[],
        }
    }

    

    componentDidMount() {
        console.log('Componnet');
    }

    editUser(event,user_id) {
        event.preventDefault();
        let target = event.target;
        let editdata = {
            name: target.name,
            email:target.email,
            department:target.department,
            job:target.job,
            password:target.password,
            role:target.role,
            user_id: Math.random().toFixed()


            // name: this.target.name.value,
            // email: this.refs.email.value,
            // department: this.refs.department.value,
            // job: this.refs.job.value,
            // password: this.refs.password.value,
            // role: this.refs.role.value,
            // user_id: Math.random().toFixed()
        }
        
        var request = new Request('http://localhost:3001/api/editUser' + user_id, {
            method: 'PUT',
            headers: new Headers({'Content-Type': 'application/json'}),
            body: JSON.stringify(editdata)
        });

        fetch(request)
            .then(function(response) {
                response.json()
                .then(function(data) {
                })
            })
            .catch(function(err) {
                console.log(err)
            })
    }

    render() {
        let user = this.state.user;
        return (
            <div>
                <div className="haeder">
                    <h2>Edit</h2>
                </div>
                <div className="form">
                   <div className="box">
                       <div className="name">
                           <h3 >Name</h3>
                           <h3 className="required"> *</h3>
                           <input type="text" required onChange={this.editUser} />
                       </div>
                       <div className="name">
                           <h3 >Email</h3>
                           <h3 className="required"> *</h3>
                           <input type="email" required onChange={this.editUser}/>
                       </div>
                       <div className="name">
                           <h3 >Department</h3>
                           <h3 className="required"> *</h3>
                           <select name="department">
                                <option onChange={this.editUser}>IT</option>
                                <option onChange={this.editUser}>acc</option>
                           </select>
                       </div>
                       <div className="name">
                           <h3 >Job title</h3>
                           <h3 className="required"> *</h3>
                           <input type="text" required onChange={this.editUser}/>
                       </div>
                       <div className="name">
                           <h3 >Password</h3>
                           <h3 className="required"> *</h3>
                           <input type="possword" required onChange={this.editUser}/>
                       </div>
                       <div className="name">
                           <h3 >Role</h3>
                           <input type="checkbox" className="check" onChange={this.editUser}value="admin"/> Admin
                           <input type="checkbox" className="check" onChange={this.editUser}value="manage"/> Manage
                       </div>
                       <div className="button">
                       {/* <button onClick={this.editUser.bind(this)}>save</button> */}
                       <Button onClick={this.editUser.bind(this)} type="primary" className="btnSave"> SAVE</Button>
                       <Button danger type="primary"><Redirect to="/UserManage" className="btnCancel">CANCEL</Redirect></Button>
                       </div>
                   </div>
                    {/* <pre>{JSON.stringify(user)}</pre> */}
                </div>
            </div>
        )
    }
}

export default AddUser;