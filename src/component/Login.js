import React from 'react';
import './staly.css';
import { Input, Result, Col } from 'antd';
import { Button } from 'antd';
import {PostData} from './PostData';
import {Redirect} from 'react-router-dom';

class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = { 
            email:'',
            password:'',
            redirect: false
        }  
       this.login = this.login.bind(this);
       this.handleOnchange = this.handleOnchange.bind(this);
    }

 handleOnchange(e) {
    this.setState({
        [e.target.name]:e.target.value
    });
    // console.log(this.state);
 }

    login() {
        // console.log(this.state.password)
        // let email = this.state
        PostData(this.state).then((result) => {
            
            let responseJson = result;
            console.log(responseJson)
            if( responseJson != '') {
                sessionStorage.setItem('userData', responseJson);
                this.setState({
                    redirect: true,
                  
                });
                // console.log("goto")
                // console.log(this.state);
            } else {
                // console.log("error");
                alert("You don’t have permission, please contact Administrator")
            }
        });
    }

    render() {

        if( this.state.redirect) {
            return <Redirect to={'/UserManage'} />
        }

        return (
            <div className="layout"> 
                <h1 className="login">LOGIN</h1>
                <div className="form">
                    <div className="box">
                        <div className="email">
                            <h3>Email</h3>
                            <Input placeholder="Email" name="email" onChange={this.handleOnchange} />
                            {/* <input ref="email" /> */}
                        </div>
                        <div className="password">
                            <h3>Password</h3>
                            <Input.Password placeholder="input password" name="password"  onChange={this.handleOnchange} />
                            {/* <input ref="password" /> */}
                        </div>
                        <div className="button">
                        <Button type="primary" onClick={this.login.bind(this)} >LOGIN</Button>
                        {/* <Button type="primary">LOGIN</Button> */}
                        </div>
                    </div>
                </div>
            </div>
        )    
    }
}

export default Login;