export function PostData(userData) {
    return new Promise( (reslove,reject) =>{
        fetch('http://localhost:3001/api/login',{
            method: 'POST',
            headers: new Headers({'Content-Type': 'application/json'}),
            body: JSON.stringify(userData)
        })
        .then((response) => response.json())
        .then((responseJson) => {
            reslove(responseJson)
        })
        .catch((error) => {
            reject(error);
        })
    });
}