import React ,{ useContext, useState, useEffect, useRef }from 'react';
import './staly.css';
import {Link} from 'react-router-dom';
import { Table, Input, Button, Form,Popconfirm, Modal} from 'antd';
import {Redirect} from 'react-router-dom';
import EditUser from './EditUser';

const EditableContext = React.createContext();

const EditableRow = ({ index, ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

const EditableCell = ({
  title,
  editable,
  children,
  dataIndex,
  record,
  handleSave,
  ...restProps
}) => {
  const [editing, setEditing] = useState(false);
  const inputRef = useRef();
  const form = useContext(EditableContext);
  useEffect(() => {
    if (editing) {
      inputRef.current.focus();
    }
  }, [editing]);

  const toggleEdit = () => {
    setEditing(!editing);
    form.setFieldsValue({
      [dataIndex]: record[dataIndex],
    });
  };

  const save = async e => {
    try {
      const values = await form.validateFields();
      toggleEdit();
      handleSave({ ...record, ...values });
    } catch (errInfo) {
      console.log('Save failed:', errInfo);
    }
  };

  let childNode = children;

  if (editable) {
    childNode = editing ? (
      <Form.Item
        style={{
          margin: 0,
        }}
        name={dataIndex}
        rules={[
          {
            required: true,
            message: `${title} is required.`,
          },
        ]}
      >
      <Input ref={inputRef} onPressEnter={save} onBlur={save} />
      </Form.Item>
    ) : (
      <div
        className="editable-cell-value-wrap"
        style={{
          paddingRight: 24,
        }}
        onClick={toggleEdit}
      >
        {children}
      </div>
    );
  }

  return <td {...restProps}>{childNode}</td>;
};
class UserManage extends React.Component {

    state = { 
      // visible: false ,
      redirect: false,
      btnEdit : false,
    };
    constructor(props) {
        super(props)
        this.columns = [
          {
            title: 'Name',
            dataIndex: 'name',
            width: '30%',
          },
          {
            title: 'Email',
            width: '30%',
            dataIndex: 'email',
          },
          {
            title: 'Department',
            dataIndex: 'department',
            width:'20%'
          },
          {
            title: 'Job Title',
            dataIndex: 'job',
            width:'20%'
          },
          {
            title: 'Admin',
            dataIndex: 'role',
            width:'10%'
          },
          {
            title: 'Manage',
            dataIndex: 'role',
            width:'10%'
          },
          {
            title: 'operation',
            dataIndex: 'operation',
            render: (text, record) => 
            (
                <span className="btnOpertion">
                    
                    <Popconfirm 
                        title="Are you Sure you want to delete?" 
                        onConfirm={() => this.handleDelete(record.user_id)}
                        okText="Yes"
                        cancelText="No"
                    >
                        <a>Delete</a>
                    </Popconfirm>
                    <p>|</p>
                    <Popconfirm 
                        title="Are you Sure you want to delete?" 
                        onConfirm={() => this.handleEdit(record.user_id)}
                        okText="Yes"
                        cancelText="No"
                    >
                        <a>Edit</a>
                    </Popconfirm>
                    {/* <Link to="/EditUser">Edit</Link>  */}
                    {/* <a disabled={editingKey !== ''} onClick={() => edit(record)}>  Edit </a> */}
                </span>
            )
          },
          
        ];
    
        this.state = {
          dataSource: [],
          count: 2,
        };
      }
    
      componentDidMount() {
          var that = this;
          fetch('http://localhost:3001/api/user')
          .then(function(response) {
              response.json()
              .then(function(data) {
                  let dataSource = that.state.dataSource;
                  dataSource.concat(data);
                  that.setState ({
                    dataSource:data
                  })
              })
          })
      }

      handleDelete(user_id) {
        var that = this;
        let dataSource = [...this.state.dataSource];
        let deleData = dataSource.find(function(record) {
            return record.user_id === user_id
        });
        console.log(dataSource)
        var request = new Request('http://localhost:3001/api/delete/' + user_id, {
            method: 'DELETE'
        });

        fetch(request)
        .then(function(response) {
          dataSource.splice(deleData, 1);
          console.log(dataSource)
          that.setState({
            dataSource : dataSource
          })
          response.json()
          .then(function(data) {
            // console.log(data)
          })
        })

      }
     
      
      handleEdit(user_id) {
        let id = this.props.user_id
        console.log(id)
        let dataSource = [...this.state.dataSource];
        this.setState({
          btnEdit : true,
        })

      }
    
      handleAdd = () => {
        this.setState({
          redirect : true
        })
       
      };
    render() {
        const { selectedRowKeys } = this.state;
        const rowSelection = {
        selectedRowKeys,
        onChange: this.onSelectChange,
        hideDefaultSelections: true,
        selections: [
            Table.SELECTION_ALL,
            Table.SELECTION_INVERT,
            {
            key: 'odd',
            text: 'Select Odd Row',
            onSelect: changableRowKeys => {
                let newSelectedRowKeys = [];
                newSelectedRowKeys = changableRowKeys.filter((key, index) => {
                if (index % 2 !== 0) {
                    return false;
                }
                return true;
                });
                this.setState({ selectedRowKeys: newSelectedRowKeys });
            },
            },
            {
            key: 'even',
            text: 'Select Even Row',
            onSelect: changableRowKeys => {
                let newSelectedRowKeys = [];
                newSelectedRowKeys = changableRowKeys.filter((key, index) => {
                if (index % 2 !== 0) {
                    return true;
                }
                return false;
                });
                this.setState({ selectedRowKeys: newSelectedRowKeys });
            },
            },
        ],
        };
        const { dataSource } = this.state;
        const components = {
            body: {
                row: EditableRow,
                cell: EditableCell,
            },
            };
            const columns = this.columns.map(col => {
            if (!col.editable) {
                return col;
            }
            return {
                ...col,
                onCell: record => ({
                record,
                editable: col.editable,
                dataIndex: col.dataIndex,
                title: col.title,
                handleSave: this.handleSave,
                }),
            };
        });
        if(this.state.redirect) {
        return <Redirect to="/AddUser" ></Redirect>
        }

        if (this.state.btnEdit) {
          // return <Redirect to="/EditUser"  /> 
        }
        return (
            <div>
                <div className="haeder">
                    <h3>User Manage</h3>
                    <div className="btnLogout">
                        <h3>Admin</h3>
                        <div className="line"></div>
                        <Link to="/"> LOGOUT</Link>
                    </div>
                </div>
                <Button onClick={this.handleAdd} type="primary" className="btnAdd">
                  ADD NEW
                </Button>
                <Table
                    components={components}
                    rowClassName={() => 'editable-row'}
                    bordered
                    dataSource={dataSource}
                    columns={columns}
                    rowSelection={rowSelection}
                />
            
            </div>
        )
    }
}


export default UserManage;